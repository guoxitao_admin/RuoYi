package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.UserTest;
import com.ruoyi.system.service.IUserTestService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 测试用户生成Controller
 *
 * @author ruoyi
 * @date 2020-09-17
 */
@Controller
@RequestMapping("/tool/SysUserGen")
public class UserTestController extends BaseController {
    private String prefix = "tool/SysUserGen";

    @Resource
    private IUserTestService userTestService;

    @RequiresPermissions("tool:SysUserGen:view")
    @GetMapping()
    public String SysUserGen() {
        return prefix + "/SysUserGen";
    }

    /**
     * 查询测试用户生成列表
     */
    @RequiresPermissions("tool:SysUserGen:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UserTest userTest) {
        startPage();
        List<UserTest> list = userTestService.selectUserTestList(userTest);
        return getDataTable(list);
    }

    /**
     * 导出测试用户生成列表
     */
    @RequiresPermissions("tool:SysUserGen:export")
    @Log(title = "测试用户生成", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UserTest userTest) {
        List<UserTest> list = userTestService.selectUserTestList(userTest);
        ExcelUtil<UserTest> util = new ExcelUtil<UserTest>(UserTest.class);
        return util.exportExcel(list, "SysUserGen");
    }

    /**
     * 新增测试用户生成
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存测试用户生成
     */
    @RequiresPermissions("tool:SysUserGen:add")
    @Log(title = "测试用户生成", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UserTest userTest) {
        return toAjax(userTestService.insertUserTest(userTest));
    }

    /**
     * 修改测试用户生成
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        UserTest userTest = userTestService.selectUserTestById(id);
        mmap.put("userTest", userTest);
        return prefix + "/edit";
    }

    /**
     * 修改保存测试用户生成
     */
    @RequiresPermissions("tool:SysUserGen:edit")
    @Log(title = "测试用户生成", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UserTest userTest) {
        return toAjax(userTestService.updateUserTest(userTest));
    }

    /**
     * 删除测试用户生成
     */
    @RequiresPermissions("tool:SysUserGen:remove")
    @Log(title = "测试用户生成", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(userTestService.deleteUserTestByIds(ids));
    }
}
