package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.UserTest;
import com.ruoyi.system.mapper.UserTestMapper;
import com.ruoyi.system.service.IUserTestService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 测试用户生成Service业务层处理
 *
 * @author ruoyi
 * @date 2020-09-17
 */
@Service
public class UserTestServiceImpl implements IUserTestService {
    @Resource
    private UserTestMapper userTestMapper;

    /**
     * 查询测试用户生成
     *
     * @param id 测试用户生成ID
     * @return 测试用户生成
     */
    @Override
    public UserTest selectUserTestById(Long id) {
        return userTestMapper.selectUserTestById(id);
    }

    /**
     * 查询测试用户生成列表
     *
     * @param userTest 测试用户生成
     * @return 测试用户生成
     */
    @Override
    public List<UserTest> selectUserTestList(UserTest userTest) {
        return userTestMapper.selectUserTestList(userTest);
    }

    /**
     * 新增测试用户生成
     *
     * @param userTest 测试用户生成
     * @return 结果
     */
    @Override
    public int insertUserTest(UserTest userTest) {
        return userTestMapper.insertUserTest(userTest);
    }

    /**
     * 修改测试用户生成
     *
     * @param userTest 测试用户生成
     * @return 结果
     */
    @Override
    public int updateUserTest(UserTest userTest) {
        return userTestMapper.updateUserTest(userTest);
    }

    /**
     * 删除测试用户生成对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserTestByIds(String ids) {
        return userTestMapper.deleteUserTestByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除测试用户生成信息
     *
     * @param id 测试用户生成ID
     * @return 结果
     */
    @Override
    public int deleteUserTestById(Long id) {
        return userTestMapper.deleteUserTestById(id);
    }
}
