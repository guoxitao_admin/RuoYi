package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.system.domain.UserTest;

/**
 * 测试用户生成Mapper接口
 *
 * @author ruoyi
 * @date 2020-09-17
 */
public interface UserTestMapper {
    /**
     * 查询测试用户生成
     *
     * @param id 测试用户生成ID
     * @return 测试用户生成
     */
    public UserTest selectUserTestById(Long id);

    /**
     * 查询测试用户生成列表
     *
     * @param userTest 测试用户生成
     * @return 测试用户生成集合
     */
    public List<UserTest> selectUserTestList(UserTest userTest);

    /**
     * 新增测试用户生成
     *
     * @param userTest 测试用户生成
     * @return 结果
     */
    public int insertUserTest(UserTest userTest);

    /**
     * 修改测试用户生成
     *
     * @param userTest 测试用户生成
     * @return 结果
     */
    public int updateUserTest(UserTest userTest);

    /**
     * 删除测试用户生成
     *
     * @param id 测试用户生成ID
     * @return 结果
     */
    public int deleteUserTestById(Long id);

    /**
     * 批量删除测试用户生成
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserTestByIds(String[] ids);
}
